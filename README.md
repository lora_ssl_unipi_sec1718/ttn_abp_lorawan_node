# ABP LoraWan
An abp activated Lorawan Node modified and optimized for single channel use.

## Hardware
The hardware used and tested is arduino UNO and a Dragino Shield that modulated the signal on 868Mhz (EU use only).

## Libraries wanted
The libraries that are needed is the LMIC that can be found on the following [repository](https://github.com/matthijskooijman/arduino-lmic).